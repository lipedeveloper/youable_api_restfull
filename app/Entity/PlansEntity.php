<?php

namespace App\Entity;


class Plans
{
    public $id;
    public $name;
    public $date_init;
    public $date_end;
    public $price;
    public $discount;
    public $date_init_discount;
    public $data_end_discount;
 
    public function insert($name, $date_init, $date_end, $price, $discount, $date_init_discount, $data_end_discount)
    {
        $this->name = $name;
        $this->date_init = $date_init;
        $this->date_end = $date_end;
        $this->price = $price;
        $this->discount = $discount;
        $this->date_init_discount = $date_init_discount;
        $this->data_end_discount = $data_end_discount;
    }
}
