<?php

namespace App\Entity;


class YoutubeVideos
{
    public $id;
    public $name;
    public $source;
    public $type;
    public $FK_youtube_category;


    public function __construct($videos = null)
    {

        if ($videos != null) {
            $this->id = $videos->id;
            $this->name = $videos->name;
            $this->source = $videos->source;
            $this->type = $videos->type;
        }
        return $this;
    }
    public function get(int $id, string $name, string $source, string $type)
    {
        $this->id = $id;
        $this->name = $name;
        $this->source = $source;
        $this->type = $type;

        return $this;
    }

    public function insert(string $name, string $source, string $type, int $FK_youtube_category)
    {
        $this->name = $name;
        $this->source = $source;
        $this->type = $type;
        $this->FK_youtube_category = $FK_youtube_category;
    }
}
