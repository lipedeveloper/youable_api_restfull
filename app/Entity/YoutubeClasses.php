<?php

namespace App\Entity;

use App\Entity\YoutubeCategory;
use Exception;

class YoutubeClasses
{
    public $id;
    public $name;
    public $class_date;
    public $detail;
    public $thumbnail;
    public $work_load;
    public $youtube_category;

    public function __construct()
    {
    }

    public function get($id, string $name, string $detail, string $thumbnail, string $work_load, $youtube_category)
    {
        $this->id = $id;
        $this->name = $name;
        $this->class_date =  date("Y-m-d H:i:s");
        $this->detail = $detail;
        $this->thumbnail = $thumbnail;
        $this->work_load = $work_load;
        $this->youtube_category = $youtube_category;

        return $this;
    }

    public function transformArray($array, $category, $videos)
    {
        $this->id = $array->id;
        $this->name = $array->name;
        $this->class_date =  date("Y-m-d H:i:s");
        $this->detail = $array->detail;
        $this->thumbnail = $array->thumbnail;
        $this->work_load = $array->work_load;
        $this->youtube_category = $this->Category($category, $videos);

        return $this;
    }

    private function Category($categorys, $videos)
    {
        $arr = [];
        foreach ($categorys as $category) {

            array_push($arr, new YoutubeCategory($category, $videos));
        }

        return $arr;
    }

    public function insert(string $name, string $detail, string $thumbnail, string $work_load)
    {
        $this->name = $name;
        $this->class_date =  date("Y-m-d H:i:s");
        $this->detail = $detail;
        $this->thumbnail = $thumbnail;
        $this->work_load = $work_load;
    }
}
