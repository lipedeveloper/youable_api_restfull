<?php

namespace App\Entity;


use App\Entity\YoutubeVideos;

class YoutubeCategory
{
    public $id;
    public $name;
    public $detail;
    public $thumbnail;
    public $youtube_videos;
    public $FK_youtube_classes;


    public function __construct($classes = null, $videos = null)
    {
        if (($classes != null) && ($videos != null)) {
            $this->id = $classes->id;
            $this->name = $classes->name;
            $this->detail = $classes->detail;
            $this->thumbnail = $classes->thumbnail;
            $this->youtube_videos = $this->Videos($videos);
        }
        return $this;
    }

    private function Videos($videos)
    {
        $arr = [];
        foreach ($videos as $video) {

            array_push($arr,  new YoutubeVideos($video));
        }

        return $arr;
    }

    public function get(int $id, string $name, string $detail, string $thumbnail, $youtube_videos)
    {
        $this->id = $id;
        $this->name = $name;
        $this->detail = $detail;
        $this->thumbnail = $thumbnail;
        $this->youtube_videos = $youtube_videos;

        return $this;
    }


    public function insert(string $name, string $detail, string $thumbnail, $FK_youtube_classes)
    {
        $this->name = $name;
        $this->detail = $detail;
        $this->thumbnail = $thumbnail;
        $this->FK_youtube_classes = $FK_youtube_classes;
    }
}
