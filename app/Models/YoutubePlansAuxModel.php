<?php

namespace App\Models;

use CodeIgniter\Model;

class YoutubePlansAuxModel extends Model
{

    protected $table      = 'youtube_plans_aux';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['FK_youtube_plans_id', 'FK_youtube_classes_id'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
