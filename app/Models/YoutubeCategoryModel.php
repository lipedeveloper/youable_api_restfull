<?php

namespace App\Models;

use CodeIgniter\Model;

class YoutubeCategoryModel extends Model
{
    protected $table      = 'youtube_category';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['name', 'detail', 'thumbnail', 'FK_youtube_classes'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function getWhereEntity(array $where, bool $specificFields = false, array $fields = ['id'])
    {
        $builder = $this->builder($this->table);

        if ($specificFields)
            $builder->select($fields);
        else
            $builder->select("*");

        $builder->where($where);
        $query = $builder->get()->getResult();
        return $query;
    }
}
