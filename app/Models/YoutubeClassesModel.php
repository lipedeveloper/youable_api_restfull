<?php

namespace App\Models;

use CodeIgniter\Model;

class YoutubeClassesModel extends Model
{

    protected $table      = 'youtube_classes';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['name', 'clas_date', 'detail', 'thumbnail', 'work_load'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
