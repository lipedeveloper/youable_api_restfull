<?php

namespace App\Models;

use CodeIgniter\Model;

class PlansModel extends Model
{

    protected $table      = 'plans';
    protected $primaryKey = 'id';

    protected $returnType = '\App\Entity\PlansEntity';
    protected $useSoftDeletes = true;

    protected $allowedFields = ["name","date_init","date_end","price","discount","date_init_discount", "data_end_discount"];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function getWhereEntity(array $where, bool $specificFields = false, array $fields = ['id'])
    {
        $builder = $this->builder($this->table);

        if ($specificFields)
            $builder->select($fields);
        else
            $builder->select("*");

        $builder->where($where);
        $query = $builder->get()->getResult();
        return $query;
    }
}
