<?php

namespace App\Controllers;

header('Access-Control-Allow-Origin: *');

use CodeIgniter\HTTP\Response;
use App\Models\UserModel;
use App\Controllers\MyController;

class User extends MyController
{

    protected $format = 'json';

    public function create()
    {

        $json = json_decode(trim(file_get_contents('php://input')), true);
        if (!empty($json)) {
            $_POST = $json;
        }

        $where = [
            'email'        => $json['email'],
            'deleted_at'   => null
        ];

        $get = $this->getByColumnWhere($where, true, ['id', 'name', 'email', 'picture', 'birthDay', 'gender', 'active'], 'users');


        if ($get != null && $get->email == $json['email']) {
            if (!$get->active)
                return $this->setResponseBase(null, false, "Email bloqueado e já cadastrado!");
            else
                return $this->setResponseBase(null, false, "Email já cadastrado!");
        }


        $userModel = new UserModel();
        $data = [
            'name'         => $json['name'],
            'email'        => $json['email'],
            'password'     => md5($json['password']),
            'picture'      => isset($json['picture']) ? $json['picture'] : "",
            'birthDay'     => isset($json['birthDay']) ? $json['birthDay'] : "",
            'gender'       => isset($json['gender']) ? $json['gender'] : "",
            'active'       => TRUE
        ];

        return $this->setResponseBase($userModel->insert($data), true, "Usuário cadastrado com sucesso!");
    }

    public function delete($id = null)
    {
        $userModel = new UserModel();
        return $this->respond($userModel->delete($id));
    }

    public function update($id = null, $state = null)
    {
        return $this->respond($this->updateBlockedUser($id, boolval($state)));
    }

    public function show($deleted = 0)
    {
        $userModel = new UserModel();
        $response = $userModel->findAllWhere(['active' => $deleted, 'deleted_at' => null]);

        return $this->setResponseBase($response);
    }
 
    public function options(): Response
    {
        return $this->response->setHeader('Access-Control-Allow-Origin', '*') //for allow any domain, insecure
            ->setHeader('Access-Control-Allow-Headers', '*') //for allow any headers, insecure
            ->setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE') //method allowed
            ->setStatusCode(200); //status code
    }
}
