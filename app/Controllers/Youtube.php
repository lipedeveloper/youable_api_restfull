<?php

namespace App\Controllers;

header('Access-Control-Allow-Origin: *');

use CodeIgniter\HTTP\Response;
use App\Models\YoutubeCategoryModel;
use App\Models\YoutubeClassesModel;
use App\Models\YoutubeVideosModel;

//Modelos
use App\Entity\YoutubeClasses;
use App\Entity\YoutubeCategory;
use App\Entity\YoutubeVideos;

use App\Controllers\MyController;
use Exception;

class Youtube extends MyController
{

    protected $format = 'json';

    public function create()
    {
       
        $json = json_decode(trim(file_get_contents('php://input')), true);
        
        foreach ($json as $classes) {
            try {
                $youtubeClassesModel = new YoutubeClassesModel();
                $ytClasses =  new YoutubeClasses();

                $ytClasses->insert($classes['Name'], $classes['Detail'], $classes['Thumbnail'], $classes['WorkLoad'], $classes['Name'], $classes['Detail'], $classes['Thumbnail'], $classes['WorkLoad']);
                
                $ytClassesId = $youtubeClassesModel->insert($ytClasses);

                foreach ($classes['YoutubeVideosCategory'] as $category) {
                    $youtubeCategoryModel = new YoutubeCategoryModel();
                    $ytCategory =  new YoutubeCategory();
                    $ytCategory->insert($category['Name'], $category['Detail'], $category['Thumbnail'], $ytClassesId);

                    $ytCategoryId = $youtubeCategoryModel->insert($ytCategory);

                    foreach ($category['YoutubeVideos'] as $videos) {
                        $youtubeVideosModel = new YoutubeVideosModel();
                        $ytVideos =  new YoutubeVideos();

                        $ytVideos->insert($videos['Name'], $videos['Source'], $videos['Type'], $ytCategoryId);
                        $youtubeVideosModel->insert($ytVideos);
                    }
                }
            } catch (Exception $ex) {
                return $this->setResponseBase($ex->getTrace(), false, $ex->getMessage());
            }
        }

        return $this->setResponseBase(null, true, "Aula cadastrada com sucesso!");
    }

    public function delete($id = null)
    {
        $youtubeClassesModel = new YoutubeClassesModel();
        return $this->respond($youtubeClassesModel->delete($id));
    }

    public function show($id = null)
    {
        $youtubeClassesModel = new YoutubeClassesModel();


        $classes = $youtubeClassesModel->findAll();
        $youtubeClasses = [];

        foreach ($classes as $class) {

            $youtubeClass = new YoutubeClasses();
            $youtubeCategoryModel = new YoutubeCategoryModel();
            $categorys = $youtubeCategoryModel->findAllWhere(["FK_youtube_classes" => $class->id]);

            foreach ($categorys as $category) {
                $youtubeVideos = new YoutubeVideosModel();
                $videos = $youtubeVideos->findAllWhere(["FK_youtube_category" => $category->id], "youtube_videos");
            }
            array_push($youtubeClasses, $youtubeClass->transformArray($class, $categorys, $videos));
        }

        return $this->setResponseBase($youtubeClasses);
    }

    public function getWhere(array $where, $table = null,  bool $specificFields = false, array $fields = ['id'])
    {
        $db = \Config\Database::connect();
        $builder = $db->table($table == null ? $this->table : $table);

        if ($specificFields)
            $builder->select($fields);
        else
            $builder->select("*");

        $builder->where($where);
        $query = $builder->get()->getResult();
        return $query;
    }

    public function getAll($table = null)
    {

        $db = \Config\Database::connect();
        $builder = $db->table($table == null ? $this->table : $table);

        $query = $builder->get()->getResultArray();
        return $query;
    }


    public function options(): Response
    {
        return $this->response->setHeader('Access-Control-Allow-Origin', '*') //for allow any domain, insecure
            ->setHeader('Access-Control-Allow-Headers', '*') //for allow any headers, insecure
            ->setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE') //method allowed
            ->setStatusCode(200); //status code
    }
}
