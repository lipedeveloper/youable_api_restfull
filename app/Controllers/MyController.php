<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class MyController extends ResourceController
{
    public function getByColumnWhere($where, $specificFields = false, $fields = ['id'], $table = null)
    {
        $db = \Config\Database::connect();
        $builder = $db->table($table == null ? $this->table : $table);

        if ($specificFields)
            $builder->select($fields);
        else
            $builder->select("*");

        $builder->where($where);
        $query = $builder->get()->getRow();
        return $query;
    }

    public function updateBlockedUser($id, bool $state)
    {
        $db = \Config\Database::connect();
        $builder = $db->table("users");
        $data = [
            'active' => $state,
        ];

        $builder->where('id', $id);
        $query = $builder->update($data);
        return $query;
    }


    public function setResponseBase($data, $success = true, $message = "")
    {
        $base = [
            'success' => $success,
            'message' => $message,
            'data' => $data
        ];

        return $this->respond($base);
    }
}
