<?php

namespace App\Controllers;

header('Access-Control-Allow-Origin: *');

use CodeIgniter\HTTP\Response;
use App\Models\PlansModel;
use App\Controllers\MyController;

class Plans extends MyController
{

    protected $format = 'json';

    public function create()
    {

        $json = json_decode(trim(file_get_contents('php://input')), true);
    
        $plansModel = new PlansModel();
        $data = [
            "name" => $json["name"], 
            "date_init" => $json["date_init"], 
            "date_end" => $json["date_end"], 
            "price" => $json["price"], 
            "discount"  => $json["discount" ], 
            "date_init_discount" => $json["date_init_discount"], 
            "date_end_discount" => $json["date_end_discount"], 
        ];

        return $this->setResponseBase($plansModel->insert($data), true, "Plano Cadastrado com Sucesso!");
    }
 
    public function options(): Response
    {
        return $this->response->setHeader('Access-Control-Allow-Origin', '*') //for allow any domain, insecure
            ->setHeader('Access-Control-Allow-Headers', '*') //for allow any headers, insecure
            ->setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE') //method allowed
            ->setStatusCode(200); //status code
    }
}
