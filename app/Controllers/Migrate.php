<?php

namespace App\Controllers;

class Migrate extends \CodeIgniter\Controller
{

  public function index()
  {
    $migrate = \Config\Services::migrations();

    try {
      $result = $migrate->latest();

      if ($result) {
        echo "Atualizado!";
      } else {
        echo "Falha!";
      }
    } catch (\Exception $e) {
      var_dump($e);
    }
  }

 

  public function regress($id = 0)
  {
    $migrate = \Config\Services::migrations();
    if ($id != 0) {
      try {
        $result = $migrate->regress($id);

        if ($result) {
          echo "Atualizado!";
        } else {
          echo "Falha!";
        }
      } catch (\Exception $e) {
        var_dump($e);
      }
    }
  }
}
