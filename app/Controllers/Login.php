<?php

namespace App\Controllers;

header('Access-Control-Allow-Origin: *');

use CodeIgniter\HTTP\Response;

class Login extends MyController
{

    protected $format = 'json';
    protected $table = "users";

    public function index()
    {
        //return $this->respond("teste");
    }

    public function create()
    {
        $json = json_decode(trim(file_get_contents('php://input')), true);

        $where = [
            'email'        => $json['email'],
            'password'     => md5($json['password']),
            'deleted_at'   => null
        ];

        if (!isset($json['email']) || !isset($json['password'])) {
            $response = $this->setResponseBase(null, false, "Email ou Senha vazios!");
        }

        $get = $this->getByColumnWhere($where, true, ['id', 'name', 'email', 'picture', 'birthDay', 'gender', 'active']);

        if ($get != null && ($get->active)) {
            $response = $this->setResponseBase($get);
        } else if ($get != null && (!$get->active)) {
            $response = $this->setResponseBase($get, false, "Email '" . $get->email . "' foi temporariamente desativado! \n\n Procure um administrador");
        } else {
            $response = $this->setResponseBase(null, false, "Email ou Senha inválido!");
        }

        return $response;
    }

    public function options(): Response
    {
        return $this->response->setHeader('Access-Control-Allow-Origin', '*') //for allow any domain, insecure
            ->setHeader('Access-Control-Allow-Headers', '*') //for allow any headers, insecure
            ->setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE') //method allowed
            ->setStatusCode(200); //status code
    }
}
