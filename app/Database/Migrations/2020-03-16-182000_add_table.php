<?php

namespace App\Database\Migrations;

class AlterAddUsers extends \CodeIgniter\Database\Migration
{

        public function up()
        {
                $this->forge->dropTable('users');

                $this->forge->addField([
                        'id'          => [
                                'type'           => 'INT',
                                'constraint'     => 5,
                                'unsigned'       => TRUE,
                                'auto_increment' => TRUE
                        ],
                        'name'       => [
                                'type'           => 'VARCHAR',
                                'constraint'     => '255',
                        ],
                        'email'      => [
                                'type'           => 'VARCHAR',
                                'constraint'     => '255',
                                // 'unique'            => TRUE
                        ],
                        'password'   => [
                                'type'           => 'VARCHAR',
                                'constraint'     => '255',
                        ],
                        'picture'    => [
                                'type'           => 'VARCHAR',
                                'constraint'     => '255',
                        ],
                        'birthDay'   => [
                                'type'           => 'DATETIME',
                        ],
                        'gender'     => [
                                'type'           => 'VARCHAR',
                                'constraint'     => '255',
                        ],
                        'active'     => [
                                'type'           => 'BIT',
                        ],
                        'created_at' => [
                                'type'           => 'TIMESTAMP',
                                'DEFAULT'        => "DEFAULT  CURRENT_TIMESTAMP",
                                'TIMESTAMP' => true
                        ],
                        'updated_at' => [
                                'type'           => 'TIMESTAMP',
                                'null'           => true,
                                'DEFAULT'        => "NULL DEFAULT  NULL",
                                'TIMESTAMP' => true

                        ],
                        'deleted_at' => [
                                'type'           => 'TIMESTAMP',
                                'null'           => true,
                                'DEFAULT'        => "NULL DEFAULT  NULL",
                                'TIMESTAMP' => true
                        ],
                ]);
                $this->forge->addKey('id', TRUE);

                $this->forge->createTable('users');
        }

        public function down()
        {
                $this->forge->dropTable('users');
        }
}
