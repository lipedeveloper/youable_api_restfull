<?php

namespace App\Database\Migrations;

class AddYoutubeVideos extends \CodeIgniter\Database\Migration
{

    public function up()
    {

        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'name'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'class_date'       => [
                'type'           => 'DATETIME',
            ],
            'detail'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'thumbnail'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'work_load'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],

            'created_at' => [
                'type'           => 'TIMESTAMP',
                'DEFAULT'        => "DEFAULT  CURRENT_TIMESTAMP",
                'TIMESTAMP' => true
            ],
            'updated_at' => [
                'type'           => 'TIMESTAMP',
                'DEFAULT'        => "DEFAULT  CURRENT_TIMESTAMP",
                'TIMESTAMP' => true

            ],
            'deleted_at' => [
                'type'           => 'TIMESTAMP',
                'null'           => true,
                'DEFAULT'        => "NULL DEFAULT  NULL",
                'TIMESTAMP' => true
            ],
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('youtube_classes');


        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'name'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'detail'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'thumbnail'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'FK_youtube_classes'       => [
                'type'           => 'INT',
                'constraint'     => 5,
            ],
            'created_at' => [
                'type'           => 'TIMESTAMP',
                'DEFAULT'        => "DEFAULT  CURRENT_TIMESTAMP",
                'TIMESTAMP' => true
            ],
            'updated_at' => [
                'type'           => 'TIMESTAMP',
                'DEFAULT'        => "DEFAULT  CURRENT_TIMESTAMP",
                'TIMESTAMP' => true

            ],
            'deleted_at' => [
                'type'           => 'TIMESTAMP',
                'null'           => true,
                'DEFAULT'        => "NULL DEFAULT  NULL",
                'TIMESTAMP' => true
            ],
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('youtube_category');

        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'name'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'source'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'thumbnail'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'type'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'FK_youtube_category'       => [
                'type'           => 'INT',
                'constraint'     => 5,
            ],
            'created_at' => [
                'type'           => 'TIMESTAMP',
                'DEFAULT'        => "DEFAULT  CURRENT_TIMESTAMP",
                'TIMESTAMP' => true
            ],
            'updated_at' => [
                'type'           => 'TIMESTAMP',
                'DEFAULT'        => "DEFAULT  CURRENT_TIMESTAMP",
                'TIMESTAMP' => true

            ],
            'deleted_at' => [
                'type'           => 'TIMESTAMP',
                'null'           => true,
                'DEFAULT'        => "NULL DEFAULT  NULL",
                'TIMESTAMP' => true
            ],
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('youtube_videos');
    }

    public function down()
    {
        $this->forge->dropTable('youtube_classes');
        $this->forge->dropTable('youtube_category');
        $this->forge->dropTable('youtube_videos');
    }
}
