<?php

namespace App\Database\Migrations;

class AddPlans extends \CodeIgniter\Database\Migration
{

    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'name'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'date_init'       => [
                'type'           => 'DATETIME',
            ],
            'date_end'       => [
                'type'           => 'DATETIME',
            ],
            'price'           => [
                'type'            => 'decimal',
            ],
            'discount'         => [
                'type'            => 'decimal',
                'null'           => true,
            ],
            'date_init_discount'       => [
                'type'           => 'DATETIME',
                'null'           => true,
            ],
            'date_end_discount'       => [
                'type'           => 'DATETIME',
                'null'           => true,
            ],
            'created_at' => [
                'type'           => 'TIMESTAMP',
                'DEFAULT'        => "DEFAULT  CURRENT_TIMESTAMP",
                'TIMESTAMP' => true
            ],
            'updated_at' => [
                'type'           => 'TIMESTAMP',
                'DEFAULT'        => "DEFAULT  CURRENT_TIMESTAMP",
                'TIMESTAMP' => true

            ],
            'deleted_at' => [
                'type'           => 'TIMESTAMP',
                'null'           => true,
                'DEFAULT'        => "NULL DEFAULT  NULL",
                'TIMESTAMP' => true
            ],
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('plans');


        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'FK_plans_id' => [
                'type'           => 'INT',
                'constraint'     => 5,
            ],
            'FK_youtube_classes_id' => [
                'type'           => 'INT',
                'constraint'     => 5,
            ],            
            'created_at' => [
                'type'           => 'TIMESTAMP',
                'DEFAULT'        => "DEFAULT  CURRENT_TIMESTAMP",
                'TIMESTAMP' => true
            ],
            'updated_at' => [
                'type'           => 'TIMESTAMP',
                'DEFAULT'        => "DEFAULT  CURRENT_TIMESTAMP",
                'TIMESTAMP' => true
            ],
            'deleted_at' => [
                'type'           => 'TIMESTAMP',
                'null'           => true,
                'DEFAULT'        => "NULL DEFAULT  NULL",
                'TIMESTAMP' => true
            ],
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('youtube_plans_aux');

        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'FK_youtube_plans_id' => [
                'type'           => 'INT',
                'constraint'     => 5,
            ],
            'FK_user_id' => [
                'type'           => 'INT',
                'constraint'     => 5,
            ],            
            'created_at' => [
                'type'           => 'TIMESTAMP',
                'DEFAULT'        => "DEFAULT  CURRENT_TIMESTAMP",
                'TIMESTAMP' => true
            ],
            'updated_at' => [
                'type'           => 'TIMESTAMP',
                'DEFAULT'        => "DEFAULT  CURRENT_TIMESTAMP",
                'TIMESTAMP' => true
            ],
            'deleted_at' => [
                'type'           => 'TIMESTAMP',
                'null'           => true,
                'DEFAULT'        => "NULL DEFAULT  NULL",
                'TIMESTAMP' => true
            ],
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('user_plans_aux');
    }

    public function down()
    {
        $this->forge->dropTable('plans');
        $this->forge->dropTable('plans_aux');
        $this->forge->createTable('user_plans_aux');
    }
}
